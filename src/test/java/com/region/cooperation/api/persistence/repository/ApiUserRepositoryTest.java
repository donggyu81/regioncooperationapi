package com.region.cooperation.api.persistence.repository;

import com.region.cooperation.api.ApiApplication;
import com.region.cooperation.api.entities.ApiUser;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApiApplication.class)
public class ApiUserRepositoryTest {

    @Autowired
    private UserRepository userRepository;


    @Ignore
    @Test
    public void testUserInsert() {
        String name = "test1";
        String password = "1234";
        BCryptPasswordEncoder bCryptPasswordEncoder = new BCryptPasswordEncoder();
        String encodedPassword = bCryptPasswordEncoder.encode(password);
        ApiUser apiUser = new ApiUser();
        apiUser.setName(name);
        apiUser.setPassword(encodedPassword);
        userRepository.save(apiUser);
    }

    @Test
    public void getUserByName_available() {
        ApiUser apiUser = userRepository.findByName("test").orElse(null);
        Assert.notNull(apiUser,"");
    }

    @Test
    public void getUserByName_unavailable() {
        ApiUser apiUser = userRepository.findByName("test_no").orElse(null);
        Assert.isNull(apiUser, "");
    }
}
