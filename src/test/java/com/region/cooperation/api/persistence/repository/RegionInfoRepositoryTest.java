package com.region.cooperation.api.persistence.repository;

import com.region.cooperation.api.ApiApplication;
import com.region.cooperation.api.entities.RegionInfo;
import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.util.Assert;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApiApplication.class)
public class RegionInfoRepositoryTest {

    @Autowired
    private RegionInfoRepository regionInfoRepository;

    @Ignore
    @Test
    public void testRegionInfoInsert() {
        RegionInfo regionInfo = new RegionInfo();
        regionInfo.setName("강릉시");
        regionInfo.setCode("kangReungCT");
        regionInfoRepository.save(regionInfo);
    }

    @Test
    public void getRegionInfoByName_available() {
        RegionInfo regionInfo = regionInfoRepository.findByName("강릉시").orElse(null);
        Assert.notNull(regionInfo,"");
        Assert.isTrue(StringUtils.equals(regionInfo.getCode(), "pdslJraXBvl"), "");
    }

    @Test
    public void getRegionInfoByName_unavailable() {
        RegionInfo regionInfo = regionInfoRepository.findByName("강릉시11").orElse(null);
        Assert.isNull(regionInfo, "");
    }

    @Test
    public void getRegionInfoByCode_available() {
        RegionInfo regionInfo = regionInfoRepository.findByCode("pdslJraXBvl").orElse(null);
        Assert.notNull(regionInfo,"");
        Assert.isTrue(StringUtils.equals(regionInfo.getName(), "강릉시"), "");
    }

    @Test
    public void getRegionInfoByCode_unavailable() {
        RegionInfo regionInfo = regionInfoRepository.findByCode("kangReungCT11").orElse(null);
        Assert.isNull(regionInfo, "");
    }

}
