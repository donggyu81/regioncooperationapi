package com.region.cooperation.api.persistence.repository;

import com.region.cooperation.api.ApiApplication;
import com.region.cooperation.api.entities.RegionInfo;
import com.region.cooperation.api.entities.RegionSupportInfo;
import com.region.cooperation.api.entities.SecondPreserveInfo;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApiApplication.class)
public class RegionSupportInfoRepositoryTest {

    @Autowired
    private RegionSupportInfoRepository regionSupportInfoRepository;

    @Autowired
    private RegionInfoRepository regionInfoRepository;

    @Ignore
    @Test
    public void testInsertData(){
        RegionSupportInfo regionSupportInfo = new RegionSupportInfo();
        RegionInfo regionInfo = regionInfoRepository.findByName("강릉시").orElse(null);
        if(regionInfo == null) {
            regionInfo = new RegionInfo();
            regionInfo.setName("강릉시");
            regionInfo.setCode(RandomStringUtils.randomAlphabetic(11));
            regionInfoRepository.save(regionInfo);
        }
        regionSupportInfo.setRegionInfo(regionInfo);
        regionSupportInfo.setSupportTarget("강릉시 소재 중소기업으로서 강릉시장이 추천한 자");
        regionSupportInfo.setUsage("운전");
        regionSupportInfo.setSupportLimit(null);
        regionSupportInfo.setSecondPreserveInfo("3%");
        regionSupportInfo.setRecommendOffice("강릉시");
        regionSupportInfo.setManageOffice("강릉지점");
        regionSupportInfo.setTakeOffice("강릉시 소재 영업점");
        regionSupportInfoRepository.save(regionSupportInfo);
    }


    @Test
    public void testFindByRegionInfoCodeData(){
        RegionSupportInfo regionSupportInfo = regionSupportInfoRepository.findByRegionInfoCode("pdslJraXBvl").orElse(null);
        Assert.assertNotNull(regionSupportInfo);
    }

    @Test
    public void testFindByRegionInfoCodeData_null(){
        RegionSupportInfo regionSupportInfo = regionSupportInfoRepository.findByRegionInfoCode("aaa").orElse(null);
        Assert.assertNull(regionSupportInfo);
    }

    @Test
    public void testFindAllWithPagable() {
        Page<RegionSupportInfo> page = regionSupportInfoRepository.findAll(PageRequest.of(0, 10, Sort.by(Sort.Direction.DESC, "supportLimit")));
        Assert.assertTrue(page.getContent() != null);
        Assert.assertTrue(page.getContent().size() == 10);
    }


}
