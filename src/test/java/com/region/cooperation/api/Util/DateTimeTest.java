/*
 * Copyright (c) 2018 LINE Corporation. All rights reserved.
 * LINE Corporation PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.region.cooperation.api.Util;

import static org.junit.Assert.assertEquals;

import java.time.LocalDateTime;

import com.region.cooperation.api.util.time.LocalDateTimeUtils;
import org.junit.Test;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class DateTimeTest {

    @Test
    public void dateTimeTest() {
        String time = "2011-12-03T10:15:30.120Z";
        LocalDateTime localDateTime = LocalDateTimeUtils.fromISOInstant(time);
        log.info("time : {}", time);
        log.info("localDateTime : {}", localDateTime);
        log.info("convert ISO_Z : {}", LocalDateTimeUtils.toUtcISOInstant(localDateTime));

        assertEquals(time, LocalDateTimeUtils.toUtcISOInstant(localDateTime));
    }

    @Test
    public void offset_to_localDateTime() {
        String time = "2015-08-13T11:34:29.622+00:00";
        String except = "2015-08-13T20:34:29.622";
        LocalDateTime parsedLocalDateTime = LocalDateTimeUtils.parse(time);
        log.info("{}", time);
        log.info("{}", parsedLocalDateTime);
        log.info("{}", except);
        assertEquals(except, parsedLocalDateTime.toString());
    }

    @Test
    public void betweenTest() {
        LocalDateTime start = LocalDateTimeUtils.parse("2018-05-15T11:34:29");
        LocalDateTime end = LocalDateTimeUtils.parse("2018-05-16T11:34:29");
        log.info("{} - {} : {}", end, start, LocalDateTimeUtils.between(start, end));
        assertEquals(1, LocalDateTimeUtils.between(start, end));
    }

    @Test
    public void epochTimeTest() {
        log.info("{}", LocalDateTimeUtils.DEFAULT_ZONE_ID);
        log.info("{}, {}", LocalDateTimeUtils.fromTimestamp(1529953620000L), LocalDateTimeUtils.fromTimestamp(1530040020000L));
        log.info("{}, {}", LocalDateTimeUtils.UTCfromTimestamp(1529953620000L), LocalDateTimeUtils.UTCfromTimestamp(1530040020000L));
        log.info("{}, {}", LocalDateTimeUtils.fromTimestamp(1529835780000L), LocalDateTimeUtils.fromTimestamp(1529835780000L));
        log.info("{}, {}", LocalDateTimeUtils.UTCfromTimestamp(1529835780000L), LocalDateTimeUtils.UTCfromTimestamp(1530007620000L));
    }
}
