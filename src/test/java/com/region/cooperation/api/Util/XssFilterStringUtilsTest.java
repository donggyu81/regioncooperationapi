package com.region.cooperation.api.Util;

import com.region.cooperation.api.util.XssFilterStringUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;

public class XssFilterStringUtilsTest {

    @Test
    public void testXssFilterStringUtils() {
        String testString = "<html></html>";
        XssFilterStringUtils.escapeXSS(testString);
        Assert.assertFalse(StringUtils.equals(testString, XssFilterStringUtils.escapeXSS(testString)));
    }

    @Test
    public void testXssFilterStringUtils_NotFiltered() {
        String testString = "html";
        XssFilterStringUtils.escapeXSS(testString);
        Assert.assertTrue(StringUtils.equals(testString, XssFilterStringUtils.escapeXSS(testString)));
    }
}
