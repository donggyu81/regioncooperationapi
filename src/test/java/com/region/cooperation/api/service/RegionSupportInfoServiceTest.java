package com.region.cooperation.api.service;

import com.region.cooperation.api.ApiApplication;
import com.region.cooperation.api.controller.dto.request.RegionSupportInfoUpdateRequest;
import com.region.cooperation.api.controller.dto.response.RegionNamesView;
import com.region.cooperation.api.controller.dto.response.RegionSupportInfoView;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApiApplication.class)
public class RegionSupportInfoServiceTest {

    @Autowired
    private RegionSupportInfoService regionSupportInfoService;

    @Test
    public void testInsertWithCsvFile() throws IOException {
        File file = new File(this.getClass().getResource("test_2.csv").getFile());
        InputStream stream =  new FileInputStream(file);
        regionSupportInfoService.insertRegionSupportInfoWithFile(stream);
    }

    @Test
    public void testGetAllRegionSupportInfoView() {
        List<RegionSupportInfoView> regionSupportInfoViews = regionSupportInfoService.getAllRegionSupportInfoView();
        Assert.assertTrue(regionSupportInfoViews.size() > 0);
    }

    @Test
    public void testGetRegionSupportView_supportRegion() {
        RegionSupportInfoView regionSupportInfoView = regionSupportInfoService.getRegionSupportView("김포시");
        Assert.assertNotNull(regionSupportInfoView);
        Assert.assertTrue(StringUtils.equals(regionSupportInfoView.getRegion(), "김포시"));
        Assert.assertTrue(StringUtils.equals(regionSupportInfoView.getLimit(), "2억원 이내"));
        Assert.assertTrue(StringUtils.equals(regionSupportInfoView.getRate(), "1.50%~2.0%"));
    }

    @Test
    public void testGetRegionSupportView_notSupport() {
        RegionSupportInfoView regionSupportInfoView = regionSupportInfoService.getRegionSupportView("Aaa");
        Assert.assertNull(regionSupportInfoView);
    }

    @Test
    public void testUpdateRegionSupportInfo_partialUpdate() {
        RegionSupportInfoUpdateRequest regionSupportInfoUpdateRequest = new RegionSupportInfoUpdateRequest();
        regionSupportInfoUpdateRequest.setRegion("강릉시");
        regionSupportInfoUpdateRequest.setRate("1%~2%");
        RegionSupportInfoView regionSupportInfoView = regionSupportInfoService.updateRegionSupportInfo(regionSupportInfoUpdateRequest);
        Assert.assertTrue(StringUtils.equals(regionSupportInfoView.getRegion(), "강릉시"));
        Assert.assertTrue(StringUtils.equals(regionSupportInfoView.getLimit(), "추천금액 이내"));
        Assert.assertTrue(StringUtils.equals(regionSupportInfoView.getRate(), "1%~2%"));
    }

    @Test
    public void testUpdateRegionSupportInfo_notAvailableData() {
        RegionSupportInfoUpdateRequest regionSupportInfoUpdateRequest = new RegionSupportInfoUpdateRequest();
        regionSupportInfoUpdateRequest.setRegion("dasdas");
        regionSupportInfoUpdateRequest.setRate("1%~2%");
        RegionSupportInfoView regionSupportInfoView = regionSupportInfoService.updateRegionSupportInfo(regionSupportInfoUpdateRequest);
        Assert.assertNull(regionSupportInfoView);
    }

    @Test
    public void testGetRegionNamesOrderBySupportLimit_ValidCount() {
        RegionNamesView regionNamesView = regionSupportInfoService.getRegionNamesOrderBySupportLimit(10);
        Assert.assertNotNull(regionNamesView);
        Assert.assertTrue(CollectionUtils.isNotEmpty(regionNamesView.getRegionNames()));
        Assert.assertTrue(regionNamesView.getRegionNames().size() == 10);
    }

    @Test
    public void testGetRegionNamesOrderBySupportLimit_noValidCount() {
        RegionNamesView regionNamesView = regionSupportInfoService.getRegionNamesOrderBySupportLimit(0);
        Assert.assertNull(regionNamesView);
    }

    @Test
    public void testGetRegionNamesOrderBySecondPreserveInfo() {
        RegionNamesView regionNamesView = regionSupportInfoService.getRegionNamesOrderBySecondPreserveInfo(1);
        Assert.assertNotNull(regionNamesView);
        Assert.assertTrue(CollectionUtils.isNotEmpty(regionNamesView.getRegionNames()));
        Assert.assertTrue(regionNamesView.getRegionNames().size() == 1);

    }

}
