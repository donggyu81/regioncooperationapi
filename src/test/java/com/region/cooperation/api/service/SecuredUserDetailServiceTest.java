package com.region.cooperation.api.service;

import com.region.cooperation.api.ApiApplication;
import com.region.cooperation.api.entities.SecuredUser;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;


@RunWith(SpringRunner.class)
@SpringBootTest(classes = ApiApplication.class)
public class SecuredUserDetailServiceTest {

    @Autowired
    private SecuredUserDetailService securedUserDetailService;

    @Test
    public void testGetValidUser() {
        UserDetails userDetails = securedUserDetailService.loadUserByUsername("test1");
        SecuredUser securedUser = (SecuredUser)userDetails;
        Collection<GrantedAuthority> authorities = securedUser.getAuthorities();

        Assert.assertTrue(StringUtils.equals(securedUser.getUsername(), "test1"));
        Assert.assertTrue(authorities.size() == 1);
    }

    @Test
    public void testGetInvalidUser() {
        try{
            UserDetails userDetails = securedUserDetailService.loadUserByUsername("test_no");
        } catch (UsernameNotFoundException e) {
            Assert.assertTrue(true);
            return;
        }
        Assert.assertTrue(false);

    }
}
