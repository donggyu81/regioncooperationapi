/*
 * Copyright (c) 2017 LINE Corporation. All rights reserved.
 * LINE Corporation PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.region.cooperation.api.configuration;

import com.region.cooperation.api.persistence.property.PersistenceProperties;
import com.zaxxer.hikari.HikariConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.zaxxer.hikari.HikariDataSource;
import org.springframework.jdbc.datasource.embedded.DataSourceFactory;

import java.io.IOException;
import java.util.Properties;

@Configuration
@EnableConfigurationProperties(PersistenceProperties.class)
public class DataSourceConfiguration {

    @Value("${db.connection.url}")
    private String url;
    @Value("${db.connection.username}")
    private String userName;
    @Value("${db.connection.password}")
    private String password;

    @Value("${db.connection.minimumIdle}")
    private Integer minimumIdle;
    @Value("${db.connection.maximumPoolSize}")
    private Integer maximumPoolSize;

    @Primary
    @Bean(name = "dataSource")
    public HikariDataSource dataSource() throws IOException {
        Properties conf = new Properties();
        conf.load(DataSourceFactory.class.getClassLoader().getResourceAsStream("hikari.properties"));
        HikariConfig hikariConfig = new HikariConfig(conf);
        hikariConfig.setJdbcUrl(url);
        hikariConfig.setUsername(userName);
        hikariConfig.setPassword(password);
        HikariDataSource dataSource = new HikariDataSource(hikariConfig);

        dataSource.setMinimumIdle(minimumIdle);
        dataSource.setMaximumPoolSize(maximumPoolSize);
        return dataSource;
    }
}
