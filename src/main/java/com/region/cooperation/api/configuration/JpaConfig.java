/*
 * Copyright (c) 2018 LINE Corporation. All rights reserved.
 * LINE Corporation PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.region.cooperation.api.configuration;

import java.io.IOException;
import java.util.Properties;

import javax.persistence.EntityManagerFactory;
import javax.sql.DataSource;

import com.region.cooperation.api.persistence.property.PersistenceProperties;
import com.region.cooperation.api.persistence.property.PersistenceUnitProperty;
import org.apache.commons.lang.StringUtils;
import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Primary;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.LazyConnectionDataSourceProxy;
import org.springframework.jdbc.datasource.embedded.DataSourceFactory;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.Database;
import org.springframework.orm.jpa.vendor.HibernateJpaDialect;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;


@Configuration
@AutoConfigureAfter(DataSourceConfiguration.class)
@EnableTransactionManagement
@EnableJpaAuditing
@EnableJpaRepositories(entityManagerFactoryRef = "apiEntityManagerFactory",
        basePackages = {"com.region.cooperation.api.persistence.repository"},
        transactionManagerRef = "apiTransactionManager")
public class JpaConfig {
    @Autowired
    private PersistenceProperties persistenceProperties;

    @Autowired
    private DataSource dataSource;

    @Primary
    @Bean(name = "apiEntityManagerFactory")
    public EntityManagerFactory apiEntityManagerFactory()  throws IOException {
        return generateEntityManagerFactory("api", dataSource);
    }

    @Primary
    @Bean(name = "apiTransactionManager")
    PlatformTransactionManager apiTransactionManager(EntityManagerFactory apiEntityManagerFactory) {
        return new JpaTransactionManager(apiEntityManagerFactory);
    }

    private EntityManagerFactory generateEntityManagerFactory(String persistenceUnitName, DataSource dataSource) throws IOException {
        final LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
        final HibernateJpaVendorAdapter vendorAdapter = new HibernateJpaVendorAdapter();
        final LazyConnectionDataSourceProxy lazyConnectionDataSourceProxy = new LazyConnectionDataSourceProxy(dataSource);
        Properties conf = new Properties();
        conf.load(DataSourceFactory.class.getClassLoader().getResourceAsStream("persistenceunit.properties"));

        vendorAdapter.setDatabase(Database.valueOf(conf.getProperty("database")));
        vendorAdapter.setGenerateDdl(new Boolean(conf.getProperty("generateDdl")));
        vendorAdapter.setShowSql(new Boolean(conf.getProperty("showSql")));

        Properties properties = new Properties();
        properties.put(AvailableSettings.HBM2DDL_AUTO, conf.getProperty("ddlAuto"));
        properties.put(AvailableSettings.SCANNER_DISCOVERY, "class, hbm");
        properties.put(AvailableSettings.PHYSICAL_NAMING_STRATEGY, conf.getProperty("namingStrategy"));
        properties.put(AvailableSettings.FORMAT_SQL, conf.getProperty("formatSql"));
        properties.put(AvailableSettings.USE_SQL_COMMENTS, conf.getProperty("useSqlComments"));

        entityManagerFactoryBean.setJpaVendorAdapter(vendorAdapter);
        entityManagerFactoryBean.setPackagesToScan(conf.getProperty("packagesToScan"));
        entityManagerFactoryBean.setDataSource(lazyConnectionDataSourceProxy);
        entityManagerFactoryBean.setPersistenceUnitName(persistenceUnitName);
        entityManagerFactoryBean.setJpaProperties(properties);
        entityManagerFactoryBean.setJpaDialect(new HibernateJpaDialect());

        entityManagerFactoryBean.afterPropertiesSet();

        return entityManagerFactoryBean.getObject();
    }
}
