/*
 * Copyright (c) 2018 LINE Corporation. All rights reserved.
 * LINE Corporation PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.region.cooperation.api.configuration;

import com.region.cooperation.api.common.client.HttpPoolProperties;
import com.region.cooperation.api.common.client.RestTemplateFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;



@Configuration
public class RestConfiguration {

    @Autowired
    protected HttpPoolProperties httpPoolProperties;

    @Autowired
    protected RestTemplateFactory restTemplateFactory;

    @Bean(name = "restTemplate")
    public RestTemplate restTemplate() {
        return restTemplateFactory.getRestTemplate(httpPoolProperties.getTtl(), httpPoolProperties.getConnectionMaxTotal(),
                httpPoolProperties.getDefaultMaxPerRoute(), httpPoolProperties.getRetryCount(),
                httpPoolProperties.getConnectTimeout(), httpPoolProperties.getReadTimeout());
    }

}
