package com.region.cooperation.api.controller;

import com.region.cooperation.api.controller.dto.request.RegionSupportInfoUpdateRequest;
import com.region.cooperation.api.controller.dto.request.RegionSupportInfoViewRequest;
import com.region.cooperation.api.controller.dto.response.ApiResponse;
import com.region.cooperation.api.controller.dto.response.RegionNamesView;
import com.region.cooperation.api.controller.dto.response.RegionSupportInfoView;
import com.region.cooperation.api.entities.RegionSupportInfo;
import com.region.cooperation.api.service.RegionSupportInfoService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

@Slf4j
@RestController
public class RegionSupportController {

    @Autowired
    private RegionSupportInfoService regionSupportInfoService;

    @PostMapping("/uploadFile")
    public ApiResponse<?> addDataWithFile(@RequestPart("file") MultipartFile file) {
        try {
            regionSupportInfoService.insertRegionSupportInfoWithFile(file.getInputStream());
        } catch(Exception e) {
            log.error("[error] insert data error", e);
            return ApiResponse.of("9999", "Error");
        }
        return ApiResponse.of("0000", "Success");
    }

    @GetMapping("/searchAll")
    public ApiResponse<?> getAllData() {
        List<RegionSupportInfoView> regionSupportInfoViews = regionSupportInfoService.getAllRegionSupportInfoView();
        if(CollectionUtils.isEmpty(regionSupportInfoViews)) {
            return ApiResponse.of("8888", "Empty data");
        } else {
            return ApiResponse.of("0000", "Success", regionSupportInfoViews);
        }
    }

    @PostMapping("/findRegion")
    public ApiResponse<?> findRegion(@RequestBody RegionSupportInfoViewRequest regionSupportInfoViewRequest) {
        RegionSupportInfoView regionSupportInfoView = regionSupportInfoService.getRegionSupportView(regionSupportInfoViewRequest.getRegion());
        if(regionSupportInfoView == null) {
            return ApiResponse.of("8888", "Empty data");
        } else {
            return ApiResponse.of("0000", "Success", regionSupportInfoView);
        }
    }

    @PostMapping("/updateData")
    public ApiResponse<?> updateData(@RequestBody RegionSupportInfoUpdateRequest regionSupportInfoUpdateRequest) {
        RegionSupportInfoView regionSupportInfoView = regionSupportInfoService.updateRegionSupportInfo(regionSupportInfoUpdateRequest);
        if(regionSupportInfoView == null) {
            return ApiResponse.of("8888", "Empty data");
        } else {
            return ApiResponse.of("0000", "Success", regionSupportInfoView);
        }
    }

    @GetMapping("/getRegionNames")
    public ApiResponse<?> getRegionNames(@RequestParam(name="count", defaultValue = "1") Integer count) {
        RegionNamesView regionNamesView = regionSupportInfoService.getRegionNamesOrderBySupportLimit(count);
        if(regionNamesView == null) {
            return ApiResponse.of("8888", "Empty data");
        } else {
            return ApiResponse.of("0000", "Success", regionNamesView);
        }
    }

    @GetMapping("/getLeastPreserveInstitute")
    public  ApiResponse<?> getLeastPreserveInstitute() {
        RegionNamesView regionNamesView = regionSupportInfoService.getRegionNamesOrderBySecondPreserveInfo(1);
        if(regionNamesView == null) {
            return ApiResponse.of("8888", "Empty data");
        } else {
            return ApiResponse.of("0000", "Success", regionNamesView);
        }
    }

}
