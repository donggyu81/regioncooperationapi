/*
 * Copyright (c) 2018 LINE Corporation. All rights reserved.
 * LINE Corporation PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.region.cooperation.api.controller.dto.response;

import java.util.Map;

import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ApiResponse<T> {

    private String returnCode;
    private String returnMessage;
    private String displayMessage;
    private String accessToken;
    private Map<String, Object> errorDetailMap;
    private T info;

    @Builder
    public ApiResponse(String returnCode, String returnMessage, String displayMessage, T info, Map<String, Object> errorDetailMap) {
        this.returnCode = returnCode;
        this.returnMessage = returnMessage;
        this.displayMessage = displayMessage;
        this.info = info;
        this.errorDetailMap = errorDetailMap;
    }

    public static ApiResponse<?> of(String returnCode, String returnMessage) {
        return valueOf(returnCode, returnMessage,  null, null);
    }

    public static <T> ApiResponse<T> of(String returnCode, String returnMessage, T info) {
        return valueOf(returnCode, returnMessage,  info, null);
    }

    public static <T> ApiResponse<T> of(String returnCode, String returnMessage, Map<String, Object> errorDetailMap) {
        return valueOf(returnCode, returnMessage,  null, errorDetailMap);
    }
    
    public static <T> ApiResponse<T> of(String returnCode, String returnMessage, T info, Map<String, Object> errorDetailMap) {
        return valueOf(returnCode, returnMessage,  info, errorDetailMap);
    }
    
    private static <T> ApiResponse<T> valueOf(String returnCode, String returnMessage, T info, Map<String, Object> errorDetailMap) {
        return ApiResponse.<T>builder()
                .returnCode(returnCode)
                .returnMessage(returnMessage)
                .info(info)
                .errorDetailMap(errorDetailMap)
                .build();
    }
}
