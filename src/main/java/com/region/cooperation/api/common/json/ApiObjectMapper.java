/*
 * Copyright (c) 2018 LINE Corporation. All rights reserved.
 * LINE Corporation PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.region.cooperation.api.common.json;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.util.ISO8601DateFormat;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;

public final class ApiObjectMapper {

    private static final CustomLocalDateTimeSerializer LOCAL_DATE_TIME_SERIALIZER = new CustomLocalDateTimeSerializer();

    private ApiObjectMapper() {}

    public static ObjectMapper newInstance() {
        final ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
        objectMapper.enable(DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL);   // 알 수 없는 Enum일 경우 null로 설정
        objectMapper.disable(SerializationFeature.WRITE_NULL_MAP_VALUES);               // map의 값이 null인거 제외
        objectMapper.disable(SerializationFeature.FAIL_ON_EMPTY_BEANS);                 // bean이 empty 일 때 fail 하지 않음

        // localDateTime serializer 등록
        JavaTimeModule javaTimeModule = new JavaTimeModule();
        javaTimeModule.addSerializer(LocalDate.class, new LocalDateSerializer(DateTimeFormatter.ISO_LOCAL_DATE));
        javaTimeModule.addDeserializer(LocalDate.class, new LocalDateDeserializer(DateTimeFormatter.ISO_LOCAL_DATE));
        objectMapper.registerModule(javaTimeModule);
        objectMapper.registerModule(getCustomModule());
        objectMapper.configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false);
        objectMapper.setDateFormat(new ISO8601DateFormat());
        return objectMapper;
    }

    private static CustomJacksonSerializeModule getCustomModule() {
        final CustomJacksonSerializeModule module = new CustomJacksonSerializeModule();
        addBidirectionalSerializer(module, LocalDateTime.class, LOCAL_DATE_TIME_SERIALIZER, LOCAL_DATE_TIME_SERIALIZER);
        return module;
    }

    private static <T> void addBidirectionalSerializer(CustomJacksonSerializeModule module, Class<T> clazz,
                                                       CustomJacksonSerializeModule.SerializeFunction<T> serializer, CustomJacksonSerializeModule.DeserializeFunction<T> deserializer) {
        module.addCustomDeserializer(clazz, deserializer);
        module.addCustomSerializer(clazz, serializer);
    }
}
