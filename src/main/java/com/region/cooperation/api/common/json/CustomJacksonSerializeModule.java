/*
 * Copyright (c) 2018 LINE Corporation. All rights reserved.
 * LINE Corporation PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.region.cooperation.api.common.json;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;

public class CustomJacksonSerializeModule extends SimpleModule {
    private static final long serialVersionUID = -419361435710652983L;

    public <T> void addCustomSerializer(Class<T> cls, SerializeFunction<T> serializeFunction) {
        JsonSerializer<T> jsonSerializer = new JsonSerializer<T>() {
            @Override
            public void serialize(T t, JsonGenerator jsonGenerator, SerializerProvider serializerProvider)
                    throws IOException {
                serializeFunction.serialize(t, jsonGenerator);
            }
        };
        addSerializer(cls, jsonSerializer);
    }

    public <T> void addCustomDeserializer(Class<T> cls, DeserializeFunction<T> deserializeFunction) {
        JsonDeserializer<T> jsonDeserializer = new JsonDeserializer<T>() {
            @Override
            public T deserialize(final JsonParser jsonParser,
                                 final DeserializationContext deserializationContext) throws IOException {
                return deserializeFunction.deserialize(jsonParser);
            }
        };
        addDeserializer(cls, jsonDeserializer);
    }

    @FunctionalInterface
    public interface SerializeFunction<T> {
        void serialize(T t, JsonGenerator jsonGenerator) throws IOException;
    }

    @FunctionalInterface
    public interface DeserializeFunction<T> {
        T deserialize(JsonParser jsonParser) throws IOException;
    }
}
