/*
 * Copyright (c) 2018 LINE Corporation. All rights reserved.
 * LINE Corporation PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.region.cooperation.api.common.client;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;


import com.region.cooperation.api.common.property.RestTemplateProperty;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.DefaultHttpRequestRetryHandler;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;


@Component
public class RestTemplateFactory {


    public RestTemplate getRestTemplate(long poolingTtl, int maxTotal, int defaultMaxPerRoute,
                                        int retryCount, int connectionTimeout, int readTimeout) {
        PoolingHttpClientConnectionManager connectionManager = new PoolingHttpClientConnectionManager(
                poolingTtl, TimeUnit.MILLISECONDS);
        connectionManager.setMaxTotal(maxTotal);
        connectionManager.setDefaultMaxPerRoute(defaultMaxPerRoute);

        DefaultHttpRequestRetryHandler retryHandler = new DefaultHttpRequestRetryHandler(retryCount, false);
        RequestConfig config = RequestConfig.custom()
                                            .build();
        CloseableHttpClient client = HttpClientBuilder.create()
                                                      .setDefaultRequestConfig(config)
                                                      .setRetryHandler(retryHandler)
                                                      .setConnectionManager(connectionManager)
                                                      .build();

        HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory(
                client);
        requestFactory.setConnectTimeout(connectionTimeout);
        requestFactory.setReadTimeout(readTimeout);

        List<ClientHttpRequestInterceptor> interceptors = new ArrayList<>();

        RestTemplate restTemplate = new RestTemplate(requestFactory);
        restTemplate.setInterceptors(interceptors);
        return restTemplate;
    }
}
