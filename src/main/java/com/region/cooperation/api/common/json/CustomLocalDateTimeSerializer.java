/*
 * Copyright (c) 2018 LINE Corporation. All rights reserved.
 * LINE Corporation PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.region.cooperation.api.common.json;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;


import lombok.extern.slf4j.Slf4j;

import static com.region.cooperation.api.util.time.LocalDateTimeUtils.toISOWithTimeZone;
import static com.region.cooperation.api.util.time.LocalDateTimeUtils.toUtcISOInstant;
import static java.time.LocalDateTime.parse;

@Slf4j
public class CustomLocalDateTimeSerializer implements CustomJacksonSerializeModule.DeserializeFunction<LocalDateTime>,
        CustomJacksonSerializeModule.SerializeFunction<LocalDateTime> {
    private ZoneId systemZoneId;

    public CustomLocalDateTimeSerializer() {
        new CustomLocalDateTimeSerializer(ZoneId.systemDefault());
    }

    public CustomLocalDateTimeSerializer(ZoneId zoneId) {
        systemZoneId = zoneId;
    }

    @Override
    public LocalDateTime deserialize(JsonParser jsonParser) throws IOException {
        return parse(jsonParser.getText());
    }

    @Override
    public void serialize(LocalDateTime localDateTime, JsonGenerator jsonGenerator) throws IOException {
        if (systemZoneId == null) {
            jsonGenerator.writeString(toUtcISOInstant(localDateTime));
        } else {
            jsonGenerator.writeString(toISOWithTimeZone(localDateTime, systemZoneId));
        }
    }
}
