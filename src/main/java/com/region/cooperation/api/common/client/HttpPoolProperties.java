package com.region.cooperation.api.common.client;


import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;


import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Component
public class HttpPoolProperties {
    @Value("${http.pool.connectionMaxTotal}")
    private Integer connectionMaxTotal;
    @Value("${http.pool.defaultMaxPerRoute}")
    private Integer defaultMaxPerRoute;
    @Value("${http.pool.connectTimeout}")
    private Integer connectTimeout;
    @Value("${http.pool.readTimeout}")
    private Integer readTimeout;
    @Value("${http.pool.retryCount}")
    private Integer retryCount;
    @Value("${http.pool.ttl}")
    private Long ttl;
}
