/*
 * Copyright (c) 2018 LINE Corporation. All rights reserved.
 * LINE Corporation PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.region.cooperation.api.common.property;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class RestTemplateProperty {
    private Integer connectionMaxTotal;
    private Integer defaultMaxPerRoute;
    private Integer connectTimeout;
    private Integer readTimeout;
    private Integer retryCount;
    private Long ttl;
}
