/*
 * Copyright (c) 2018 LINE Corporation. All rights reserved.
 * LINE Corporation PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.region.cooperation.api.persistence.property;

import java.util.List;

import org.hibernate.resource.jdbc.spi.PhysicalConnectionHandlingMode;
import org.springframework.orm.jpa.vendor.Database;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class PersistenceUnitProperty {
    private Boolean generateDdl;
    private Database database;
    private Boolean showSql;
    private Boolean formatSql;
    private Boolean useSqlComments;
    private String ddlAuto;
    private PhysicalConnectionHandlingMode handlingMode;
    private String namingStrategy;
    private List<String> packagesToScan;
    private String currentSessionContextClass;
    private String cacheMode;
}
