package com.region.cooperation.api.persistence.repository;

import com.region.cooperation.api.entities.RegionSupportInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RegionSupportInfoRepository extends JpaRepository<RegionSupportInfo, Long> {
    Optional<RegionSupportInfo> findByRegionInfoCode(String code);

    Optional<RegionSupportInfo> findByRegionInfoName(String name);
}
