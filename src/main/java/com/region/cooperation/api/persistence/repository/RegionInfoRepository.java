package com.region.cooperation.api.persistence.repository;

import com.region.cooperation.api.entities.RegionInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RegionInfoRepository extends JpaRepository<RegionInfo, String> {

    Optional<RegionInfo> findByName(String name);

    Optional<RegionInfo> findByCode(String code);

}
