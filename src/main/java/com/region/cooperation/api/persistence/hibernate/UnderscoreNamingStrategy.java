/*
 * Copyright (c) 2018 LINE Corporation. All rights reserved.
 * LINE Corporation PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.region.cooperation.api.persistence.hibernate;

import static com.google.common.base.CaseFormat.LOWER_CAMEL;
import static com.google.common.base.CaseFormat.LOWER_UNDERSCORE;
import static com.google.common.base.CaseFormat.UPPER_UNDERSCORE;

import org.apache.commons.lang.StringUtils;
import org.hibernate.boot.model.naming.Identifier;
import org.hibernate.boot.model.naming.PhysicalNamingStrategyStandardImpl;
import org.hibernate.engine.jdbc.env.spi.JdbcEnvironment;

public class UnderscoreNamingStrategy extends PhysicalNamingStrategyStandardImpl {

    private static final long serialVersionUID = 7164364420446934690L;

    @Override
    public Identifier toPhysicalColumnName(Identifier name, JdbcEnvironment context) {
        return convertCamelToUnderScore(name);
    }

    @Override
    public Identifier toPhysicalTableName(Identifier name, JdbcEnvironment context) {
        return convertCamelToUpperUnderscore(name);
    }

    private static Identifier convertCamelToUnderScore(Identifier identifier) {
        if (identifier == null || StringUtils.isBlank(identifier.getText())) {
            return identifier;
        }
        return new Identifier(LOWER_CAMEL.to(LOWER_UNDERSCORE, identifier.getText()), identifier.isQuoted());
    }

    private static Identifier convertCamelToUpperUnderscore(Identifier identifier) {
        if (identifier == null || StringUtils.isBlank(identifier.getText())) {
            return identifier;
        }
        return new Identifier(LOWER_CAMEL.to(UPPER_UNDERSCORE, identifier.getText()), identifier.isQuoted());
    }
}
