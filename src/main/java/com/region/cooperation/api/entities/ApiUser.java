package com.region.cooperation.api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "user")
public class ApiUser extends IdentificationEntity {

    @Column(name = "name")
    private String name;

    @Column(name = "password")
    private String password;

}
