package com.region.cooperation.api.entities;

import com.region.cooperation.api.persistence.converter.SecondPreserveInfoConverter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "region_support_info")
public class RegionSupportInfo extends IdentificationEntity {

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "region_code", referencedColumnName = "code")
    private RegionInfo regionInfo;

    @Column(name = "support_target")
    private String supportTarget;

    @Column(name = "support_usage")
    private String usage;

    @Column(name = "support_limit")
    private BigDecimal supportLimit;

    @Column(name = "second_preserve")
    private String secondPreserveInfo;

    @Column(name = "recommend_office")
    private String recommendOffice;

    @Column(name = "manage_office")
    private String manageOffice;

    @Column(name = "take_office")
    private String takeOffice;

}
