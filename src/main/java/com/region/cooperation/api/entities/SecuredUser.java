package com.region.cooperation.api.entities;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

import java.util.Collection;

public class SecuredUser extends User {

    public SecuredUser(ApiUser apiUser, Collection<GrantedAuthority> grantedAuthorities) {
        super(apiUser.getName(), apiUser.getPassword(), grantedAuthorities);
    }
}
