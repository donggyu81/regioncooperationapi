package com.region.cooperation.api.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "region_info")
public class RegionInfo implements Serializable {

    @Id
    @Column(name = "name")
    private String name;

    @Column(name = "code")
    private String code;
}
