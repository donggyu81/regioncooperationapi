package com.region.cooperation.api.service;

import com.google.common.collect.Lists;
import com.region.cooperation.api.entities.ApiUser;
import com.region.cooperation.api.entities.SecuredUser;
import com.region.cooperation.api.persistence.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class SecuredUserDetailService implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String userName) throws UsernameNotFoundException {
        ApiUser apiUser = userRepository.findByName(userName).orElse(null);
        if (apiUser == null) {
            throw new UsernameNotFoundException("[Investigation] User is NotFound [" + userName + "]");
        }
        GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ADMIN");
        List<GrantedAuthority> grantedAuthorities = Lists.newArrayList(grantedAuthority);
        SecuredUser securedUser = new SecuredUser(apiUser, grantedAuthorities);
        return securedUser;
    }
}
