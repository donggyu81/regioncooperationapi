package com.region.cooperation.api.service;

import com.google.common.collect.Lists;
import com.opencsv.CSVReader;
import com.region.cooperation.api.controller.dto.request.RegionSupportInfoUpdateRequest;
import com.region.cooperation.api.controller.dto.response.RegionNamesView;
import com.region.cooperation.api.controller.dto.response.RegionSupportInfoView;
import com.region.cooperation.api.entities.RegionInfo;
import com.region.cooperation.api.entities.RegionSupportInfo;
import com.region.cooperation.api.persistence.repository.RegionInfoRepository;
import com.region.cooperation.api.persistence.repository.RegionSupportInfoRepository;
import com.region.cooperation.api.util.RegionSupportInfoViewConvertUtil;
import com.region.cooperation.api.util.comparator.RegionSupportComparatorWithSecondPreserveInfo;
import com.region.cooperation.api.util.comparator.RegionSupportComparatorWithSupportLimit;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@Service
public class RegionSupportInfoService {

    @Autowired
    private RegionSupportInfoRepository regionSupportInfoRepository;

    @Autowired
    private RegionInfoRepository regionInfoRepository;


    public Integer insertRegionSupportInfoWithFile(InputStream stream) {
        CSVReader bf = null;
        Integer insertCount = 0;
        try {
            bf = new CSVReader(new InputStreamReader(stream, StandardCharsets.UTF_8));
            String [] line = bf.readNext();
            String [] splitted = null;
            while (( splitted = bf.readNext()) != null) {
                if(splitted.length < 9) {
                    log.error("insert data errors : {}", splitted);
                    continue;
                }
                RegionSupportInfo regionSupportInfo = regionSupportInfoRepository.findByRegionInfoName(splitted[1]).orElse(null);
                if(regionSupportInfo == null) {
                    regionSupportInfo = new RegionSupportInfo();
                    RegionInfo regionInfo = regionInfoRepository.findByName(splitted[1]).orElse(null);
                    if(regionInfo == null) {
                        regionInfo = new RegionInfo();
                        regionInfo.setName(splitted[1]);
                        regionInfo.setCode(RandomStringUtils.randomAlphabetic(11));
                        regionInfoRepository.save(regionInfo);
                    }
                    regionSupportInfo.setRegionInfo(regionInfo);
                    regionSupportInfo.setSupportTarget(splitted[2]);
                    regionSupportInfo.setUsage(splitted[3]);
                    regionSupportInfo.setSupportLimit(RegionSupportInfoViewConvertUtil.castStringToSupportLimitValue(splitted[4]));
                    regionSupportInfo.setSecondPreserveInfo(splitted[5]);
                    regionSupportInfo.setRecommendOffice(splitted[6]);
                    regionSupportInfo.setManageOffice(splitted[7]);
                    regionSupportInfo.setTakeOffice(splitted[8]);
                    regionSupportInfoRepository.save(regionSupportInfo);
                } else {
                    log.error("data save skipped!!! line: {}", line);
                    continue;
                }
            }
        } catch(IOException io) {
            log.error("IO Exception occurs!!! exception : {}", io.getMessage());
        } finally {
            IOUtils.closeQuietly(bf);
        }

        return insertCount;

    }

    public List<RegionSupportInfoView> getAllRegionSupportInfoView() {
        List<RegionSupportInfo> regionSupportInfos = regionSupportInfoRepository.findAll();
        return regionSupportInfos.stream().map(it -> RegionSupportInfoViewConvertUtil.convert(it)).collect(Collectors.toList());
    }

    public RegionSupportInfoView getRegionSupportView(String regionName) {
        RegionSupportInfo regionSupportInfo = regionSupportInfoRepository.findByRegionInfoName(regionName).orElse(null);
        RegionSupportInfoView regionSupportInfoView = RegionSupportInfoViewConvertUtil.convert(regionSupportInfo);
        return regionSupportInfoView;
    }

    public RegionSupportInfoView updateRegionSupportInfo(RegionSupportInfoUpdateRequest regionSupportInfoUpdateRequest) {
        RegionSupportInfo regionSupportInfo = regionSupportInfoRepository.findByRegionInfoName(regionSupportInfoUpdateRequest.getRegion()).orElse(null);
        if(regionSupportInfo == null) {
            return null; // TODO :: error code handling
        }
        if(StringUtils.isNotBlank(regionSupportInfoUpdateRequest.getTarget())) {
            regionSupportInfo.setSupportTarget(regionSupportInfoUpdateRequest.getTarget());
        }
        if(StringUtils.isNotBlank(regionSupportInfoUpdateRequest.getUsage())) {
            regionSupportInfo.setUsage(regionSupportInfoUpdateRequest.getUsage());
        }
        if(StringUtils.isNotBlank(regionSupportInfoUpdateRequest.getLimit())) {
            regionSupportInfo.setSupportLimit(RegionSupportInfoViewConvertUtil.castStringToSupportLimitValue(regionSupportInfoUpdateRequest.getLimit()));
        }
        if(StringUtils.isNotBlank(regionSupportInfoUpdateRequest.getRate())) {
            regionSupportInfo.setSecondPreserveInfo(regionSupportInfoUpdateRequest.getRate());
        }
        if(StringUtils.isNotBlank(regionSupportInfoUpdateRequest.getInstitute())) {
            regionSupportInfo.setRecommendOffice(regionSupportInfoUpdateRequest.getInstitute());
        }
        if(StringUtils.isNotBlank(regionSupportInfoUpdateRequest.getMgmt())) {
            regionSupportInfo.setManageOffice(regionSupportInfoUpdateRequest.getMgmt());
        }
        if(StringUtils.isNotBlank(regionSupportInfoUpdateRequest.getReception())) {
            regionSupportInfo.setTakeOffice(regionSupportInfoUpdateRequest.getReception());
        }
        regionSupportInfoRepository.save(regionSupportInfo);
        return RegionSupportInfoViewConvertUtil.convert(regionSupportInfo);
    }

    public RegionNamesView getRegionNamesOrderBySupportLimit(Integer count){
        if(count == null || count <= 0) {
            return null; // TODO :: error code handling
        }
        RegionNamesView regionNamesView = new RegionNamesView();
        Page<RegionSupportInfo> pageResult = regionSupportInfoRepository.findAll(PageRequest.of(0, count, Sort.by(Sort.Direction.DESC, "supportLimit")));
        List<RegionSupportInfo> sortableList = Lists.newArrayList(pageResult.getContent());
        if(CollectionUtils.isEmpty(sortableList)) {
            return null; // TODO :: error code handling
        }
        Collections.sort(sortableList, new RegionSupportComparatorWithSupportLimit());
        List<String> names = sortableList.stream().map(it -> it.getRegionInfo().getName()).collect(Collectors.toList());
        regionNamesView.setRegionNames(names);
        return regionNamesView;
    }

    public RegionNamesView getRegionNamesOrderBySecondPreserveInfo(Integer count){
        if(count == null || count <= 0) {
            return null; // TODO :: error code handling
        }
        RegionNamesView regionNamesView = new RegionNamesView();
        List<RegionSupportInfo> regionSupportInfos = regionSupportInfoRepository.findAll();;
         if(CollectionUtils.isEmpty(regionSupportInfos)) {
            return null; // TODO :: error code handling
        }
        Collections.sort(regionSupportInfos, new RegionSupportComparatorWithSecondPreserveInfo());
        List<RegionSupportInfo> sortedResult = regionSupportInfos.subList(0, count);
        List<String> names = sortedResult.stream().map(it -> it.getRecommendOffice()).collect(Collectors.toList());
        regionNamesView.setRegionNames(names);
        return regionNamesView;
    }

}
