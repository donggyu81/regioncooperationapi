package com.region.cooperation.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

@Slf4j
@SpringBootApplication
@EnableConfigurationProperties
//@EnableAuthorizationServer
//@EnableResourceServer
public class ApiApplication {

    public static void main(String[] args) {
        SpringApplication app = new SpringApplication(ApiApplication.class);
        app.addListeners(new PosApplicationListener());
        app.run(args);
    }

    public static class PosApplicationListener implements ApplicationListener<ApplicationEvent> {

        @Override
        public void onApplicationEvent(ApplicationEvent applicationEvent) {
            if (applicationEvent instanceof ApplicationReadyEvent) {

                ApplicationReadyEvent applicationReadyEvent = (ApplicationReadyEvent) applicationEvent;
                ApplicationContext context = applicationReadyEvent.getApplicationContext();
                log.info("Application is ready.");
            } else if (applicationEvent instanceof ContextClosedEvent) {
                ContextClosedEvent contextClosedEvent = (ContextClosedEvent) applicationEvent;
                ApplicationContext context = contextClosedEvent.getApplicationContext();
                log.info("Application shuts down.");
            }
        }
    }

}
