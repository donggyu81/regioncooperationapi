/*
 * Copyright (c) 2018 LINE Corporation. All rights reserved.
 * LINE Corporation PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.region.cooperation.api.util.time;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;

public final class LocalDateUtils {

    private static final ZoneId DEFAULT_ZONE_ID = ZoneId.systemDefault();
    private static final ZoneId UTC_ZONE_ID = ZoneId.of("UTC");
    private static final DateTimeFormatter SIMPLE_DATE_FORMAT = new DateTimeFormatterBuilder().appendPattern(
            "yyyyMMdd").toFormatter();
    private static final DateTimeFormatter DOT_DATE_FORMAT = new DateTimeFormatterBuilder().appendPattern(
            "yyyy.MM.dd").toFormatter();

    public static String simpleDate() {
        return localDateToString(LocalDate.now(), SIMPLE_DATE_FORMAT);
    }

    public static String dotDate() {
        return localDateToString(LocalDate.now(), DOT_DATE_FORMAT);
    }

    private static String localDateToString(LocalDate localDate, DateTimeFormatter formatter) {
        return localDate.format(formatter);
    }

    public static LocalDate fromSimpleLocalDate(String str) {
        return stringToLocalDate(str, SIMPLE_DATE_FORMAT);
    }

    public static String toSimpleDate(LocalDate localDate) {
        return localDateTimeToString(localDate, SIMPLE_DATE_FORMAT);
    }

    public static String toDotDate(LocalDate localDate) {
        return localDateTimeToString(localDate, DOT_DATE_FORMAT);
    }

    public static LocalDate fromISOLocalDate(String str) {
        return stringToLocalDate(str, DateTimeFormatter.ISO_DATE);
    }

    private static LocalDate stringToLocalDate(String str, DateTimeFormatter formatter) {
        return LocalDate.parse(str, formatter.withZone(DEFAULT_ZONE_ID));
    }

    private static String localDateTimeToString(LocalDate localDate, DateTimeFormatter formatter) {
        return localDate.format(formatter);
    }
}
