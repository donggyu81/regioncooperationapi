package com.region.cooperation.api.util.comparator;

import com.region.cooperation.api.entities.RegionSupportInfo;
import com.region.cooperation.api.entities.SecondPreserveInfo;
import com.region.cooperation.api.util.SecondPreserveInfoConvertUtil;

import java.math.BigDecimal;
import java.util.Comparator;

public class RegionSupportComparatorWithSupportLimit implements Comparator<RegionSupportInfo> {

    @Override
    public int compare(RegionSupportInfo o1, RegionSupportInfo o2) {
        if(o1.getSupportLimit().compareTo(o2.getSupportLimit()) > 0) {
            return -1;
        } else if (o1.getSupportLimit().compareTo(o2.getSupportLimit()) < 0) {
            return 1;
        } else {
            return compareWithSecondPreserveAvg(o1, o2);
        }
    }

    private int compareWithSecondPreserveAvg(RegionSupportInfo o1, RegionSupportInfo o2) {
        SecondPreserveInfo secondPreserveInfoInFirst = SecondPreserveInfoConvertUtil.convertToSecondPreserveInfo(o1.getSecondPreserveInfo());
        SecondPreserveInfo secondPreserveInfoInSecond = SecondPreserveInfoConvertUtil.convertToSecondPreserveInfo(o2.getSecondPreserveInfo());
        BigDecimal avgValueSecondPreserveInfoInFirst = secondPreserveInfoInFirst.getLeastPreserveAmount().add(secondPreserveInfoInFirst.getMaxPreserveAmount()).divide(new BigDecimal("2"), 0, BigDecimal.ROUND_HALF_UP);
        BigDecimal avgValueSecondPreserveInfoInSecond = secondPreserveInfoInSecond.getLeastPreserveAmount().add(secondPreserveInfoInSecond.getMaxPreserveAmount()).divide(new BigDecimal("2"), 0, BigDecimal.ROUND_HALF_UP);
        if(avgValueSecondPreserveInfoInFirst.compareTo(avgValueSecondPreserveInfoInSecond) < 0) {
            return -1;
        } else if (avgValueSecondPreserveInfoInFirst.compareTo(avgValueSecondPreserveInfoInSecond) > 0){
            return 1;
        } else {
            return 0;
        }
    }
}
