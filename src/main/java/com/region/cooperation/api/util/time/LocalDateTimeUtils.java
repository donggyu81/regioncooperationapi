package com.region.cooperation.api.util.time;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Date;

import org.springframework.util.StringUtils;

public final class LocalDateTimeUtils {

    public static final ZoneId DEFAULT_ZONE_ID = ZoneId.systemDefault();
    private static final ZoneId UTC_ZONE_ID = ZoneId.of("UTC");
    private static final String OFFSET_PARSE_TEXT = "+";
    private static final String Z_PARSE_TEXT = "Z";
    private static final DateTimeFormatter SIMPLE_DATE_TIME_FORMAT =
            new DateTimeFormatterBuilder().appendPattern("yyyyMMddHHmmss").toFormatter();
    private static final DateTimeFormatter SIMPLE_TIME_FORMAT = new DateTimeFormatterBuilder()
            .appendPattern("hh:mm").toFormatter();

    private LocalDateTimeUtils() {
        throw new UnsupportedOperationException();
    }

    public static LocalDateTime fromTimestamp(long timestamp) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), DEFAULT_ZONE_ID);
    }

    public static LocalDateTime UTCfromTimestamp(long timestamp) {
        return LocalDateTime.ofInstant(Instant.ofEpochMilli(timestamp), UTC_ZONE_ID);
    }

    public static LocalDateTime fromDate(Date date) {
        return LocalDateTime.ofInstant(date.toInstant(), DEFAULT_ZONE_ID);
    }

    public static Date toDate(LocalDateTime localDateTime) {
        return Date.from(Instant.from(localDateTime.atZone(DEFAULT_ZONE_ID)));
    }

    // 2011-12-03T10:15:30+00:00
    // 2011-12-03T10:15:30Z
    // 2011-12-03T10:15:30
    public static LocalDateTime parse(String str) {
        if (StringUtils.isEmpty(str)) {
            return null;
        } else if (str.contains(OFFSET_PARSE_TEXT)) {
            return fromISOOffset(str);
        } else if (str.contains(Z_PARSE_TEXT)) {
            return fromISOInstant(str);
        }
        return fromISOLocalDateTime(str);
    }

    // 2011-12-03T10:15:30+00:00
    public static LocalDateTime fromISOOffset(String str) {
        return ZonedDateTime.parse(str, DateTimeFormatter.ISO_OFFSET_DATE_TIME).withZoneSameInstant(
                DEFAULT_ZONE_ID).toLocalDateTime();
    }

    // 2011-12-03T10:15:30Z
    public static LocalDateTime fromISOInstant(String str) {
        return stringToLocalDateTime(str, DateTimeFormatter.ISO_INSTANT);
    }

    // 2011-12-03T10:15:30
    public static LocalDateTime fromISOLocalDateTime(String str) {
        return stringToLocalDateTime(str, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    public static String toISOInstant(LocalDateTime localDateTime) {
        return localDateTimeToString(localDateTime, DateTimeFormatter.ISO_INSTANT);
    }

    public static String toISOInstant(ZonedDateTime zonedDateTime) {
        return zonedDateTime.format(DateTimeFormatter.ISO_INSTANT);
    }

    public static String toUtcISOInstant(LocalDateTime localDateTime) {
        return toISOWithTimeZone(localDateTime, UTC_ZONE_ID);
    }

    public static String toISOWithTimeZone(LocalDateTime localDateTime, ZoneId zoneId) {
        return toISOInstant(localDateTime.atZone(ZoneId.systemDefault()).withZoneSameInstant(zoneId));
    }

    public static String toISOLocalDateTime(LocalDateTime localDateTime) {
        return localDateTimeToString(localDateTime, DateTimeFormatter.ISO_LOCAL_DATE_TIME);
    }

    public static String toSimpleDateTime(LocalDateTime localDateTime) {
        return localDateTimeToString(localDateTime, SIMPLE_DATE_TIME_FORMAT);
    }

    public static String toSimpleTimeString(LocalDateTime localDateTime) {
        return localDateTimeToString(localDateTime, SIMPLE_TIME_FORMAT);
    }

    private static LocalDateTime stringToLocalDateTime(String str, DateTimeFormatter formatter) {
        return LocalDateTime.parse(str, formatter.withZone(DEFAULT_ZONE_ID));
    }

    private static String localDateTimeToString(LocalDateTime localDateTime, DateTimeFormatter formatter) {
        return localDateTime.format(formatter);
    }

    public static long between(LocalDateTime start, LocalDateTime end) {
        return Duration.between(start, end).toDays();
    }

    public static long betweenMills(LocalDateTime start, LocalDateTime end) {
        return Duration.between(start, end).toMillis();
    }
}
