package com.region.cooperation.api.util;

import com.region.cooperation.api.entities.SecondPreserveInfo;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;

@Slf4j
public class SecondPreserveInfoConvertUtil {

    private static final String PERCENT_STRING = "%";

    private static final String MAX_VALUE = "대출이자 전액";

    private static final String TILDE_STRING = "~";

    public static SecondPreserveInfo convertToSecondPreserveInfo(String s) {
        SecondPreserveInfo secondPreserveInfo = new SecondPreserveInfo();
        if(StringUtils.isBlank(s) || StringUtils.equals(s, MAX_VALUE)) {
            secondPreserveInfo.setLeastPreserveAmount(new BigDecimal(100));
            secondPreserveInfo.setMaxPreserveAmount(new BigDecimal(100));
            return secondPreserveInfo;
        }
        try {
            if(StringUtils.contains(s, TILDE_STRING)) {
                String[] values = StringUtils.split(s, TILDE_STRING);
                BigDecimal firstValue = new BigDecimal(StringUtils.remove(values[0], PERCENT_STRING));
                BigDecimal secondValue = new BigDecimal(StringUtils.remove(values[1], PERCENT_STRING));
                if(firstValue.compareTo(secondValue) > 0) {
                    secondPreserveInfo.setLeastPreserveAmount(secondValue);
                    secondPreserveInfo.setMaxPreserveAmount(firstValue);
                } else {
                    secondPreserveInfo.setLeastPreserveAmount(firstValue);
                    secondPreserveInfo.setMaxPreserveAmount(secondValue);
                }
            } else {
                String parsedValue = StringUtils.remove(s, PERCENT_STRING);
                secondPreserveInfo.setLeastPreserveAmount(new BigDecimal(parsedValue));
                secondPreserveInfo.setMaxPreserveAmount(new BigDecimal(parsedValue));
            }

        } catch (Exception e) {
            log.error("[ERROR] parse value error. parsing value : {}, Exception : {}", s, e.getMessage());
            throw e;
        }

        return secondPreserveInfo;
    }

    public static String convertToString(SecondPreserveInfo secondPreserveInfo) {
        if(secondPreserveInfo.getLeastPreserveAmount() == null && secondPreserveInfo.getMaxPreserveAmount() == null) {
            return MAX_VALUE;
        }

        if(secondPreserveInfo.getLeastPreserveAmount() == null) {
            return secondPreserveInfo.getMaxPreserveAmount().toString() + PERCENT_STRING;
        } else if (secondPreserveInfo.getMaxPreserveAmount() == null) {
            return secondPreserveInfo.getLeastPreserveAmount().toString() + PERCENT_STRING;
        } else if(secondPreserveInfo.getLeastPreserveAmount().compareTo(secondPreserveInfo.getMaxPreserveAmount()) == 0) {
            return secondPreserveInfo.getMaxPreserveAmount().toString() + PERCENT_STRING;
        }

        return secondPreserveInfo.getLeastPreserveAmount().toString() + PERCENT_STRING + TILDE_STRING + secondPreserveInfo.getMaxPreserveAmount().toString() + PERCENT_STRING ;
    }

}
