package com.region.cooperation.api.util.comparator;

import com.region.cooperation.api.entities.RegionSupportInfo;
import com.region.cooperation.api.entities.SecondPreserveInfo;
import com.region.cooperation.api.util.SecondPreserveInfoConvertUtil;

import java.math.BigDecimal;
import java.util.Comparator;

public class RegionSupportComparatorWithSecondPreserveInfo implements Comparator<RegionSupportInfo> {

    @Override
    public int compare(RegionSupportInfo o1, RegionSupportInfo o2) {
        SecondPreserveInfo secondPreserveInfoInFirst = SecondPreserveInfoConvertUtil.convertToSecondPreserveInfo(o1.getSecondPreserveInfo());
        SecondPreserveInfo secondPreserveInfoInSecond = SecondPreserveInfoConvertUtil.convertToSecondPreserveInfo(o2.getSecondPreserveInfo());
        if(secondPreserveInfoInFirst.getLeastPreserveAmount().compareTo(secondPreserveInfoInSecond.getLeastPreserveAmount()) > 0) {
            return 1;
        } else if(secondPreserveInfoInFirst.getLeastPreserveAmount().compareTo(secondPreserveInfoInSecond.getLeastPreserveAmount()) < 0) {
            return -1;
        } else {
            return compareWithMaxPreserveAmount(secondPreserveInfoInFirst, secondPreserveInfoInSecond);
        }
    }

    private int compareWithMaxPreserveAmount(SecondPreserveInfo secondPreserveInfoInFirst, SecondPreserveInfo secondPreserveInfoInSecond) {
        if(secondPreserveInfoInFirst.getMaxPreserveAmount().compareTo(secondPreserveInfoInSecond.getMaxPreserveAmount()) > 0) {
            return 1;
        } else if (secondPreserveInfoInFirst.getMaxPreserveAmount().compareTo(secondPreserveInfoInSecond.getMaxPreserveAmount()) < 0){
            return -1;
        } else {
            return 0;
        }
    }
}
