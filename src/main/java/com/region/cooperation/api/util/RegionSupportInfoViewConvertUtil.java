package com.region.cooperation.api.util;

import com.region.cooperation.api.controller.dto.response.RegionSupportInfoView;
import com.region.cooperation.api.entities.RegionSupportInfo;
import org.apache.commons.lang.StringUtils;

import java.math.BigDecimal;

public class RegionSupportInfoViewConvertUtil {

    private final static String NULL_VALUE_KEYWORD = "추천금액 이내";

    private final static String BILLION_VALUE_KEYWORD = "억원 이내";

    private final static String MILLION_VALUE_KEYWORD = "백만원 이내";


    public static RegionSupportInfoView convert(RegionSupportInfo regionSupportInfo) {
        if(regionSupportInfo == null) {
            return null;
        }
        RegionSupportInfoView regionSupportInfoView = new RegionSupportInfoView();
        regionSupportInfoView.setRegion(regionSupportInfo.getRegionInfo().getName());
        regionSupportInfoView.setTarget(regionSupportInfo.getSupportTarget());
        regionSupportInfoView.setUsage(regionSupportInfo.getUsage());
        regionSupportInfoView.setLimit(castSupportLimitValueToString(regionSupportInfo.getSupportLimit()));
        regionSupportInfoView.setRate(regionSupportInfo.getSecondPreserveInfo());
        regionSupportInfoView.setInstitute(regionSupportInfo.getRecommendOffice());
        regionSupportInfoView.setMgmt(regionSupportInfo.getManageOffice());
        regionSupportInfoView.setReception(regionSupportInfo.getTakeOffice());
        return regionSupportInfoView;
    }

    public static BigDecimal castStringToSupportLimitValue(String supportLimitValueString) {
        if(StringUtils.equals(supportLimitValueString, NULL_VALUE_KEYWORD) == false) {
            String filterdValue = StringUtils.EMPTY;
            if(StringUtils.contains(supportLimitValueString, BILLION_VALUE_KEYWORD)) {
                filterdValue = StringUtils.remove(supportLimitValueString, BILLION_VALUE_KEYWORD);
                return new BigDecimal(filterdValue).multiply(new BigDecimal(100));
            } else if(StringUtils.contains(supportLimitValueString, MILLION_VALUE_KEYWORD)){
                filterdValue = StringUtils.remove(supportLimitValueString, MILLION_VALUE_KEYWORD);
                return new BigDecimal(filterdValue);
            }
        }
        return null;
    }

    public static String castSupportLimitValueToString(BigDecimal supportLimitValue) {
        if(supportLimitValue != null) {
            BigDecimal numberToBeView = supportLimitValue;
            if(supportLimitValue.compareTo(new BigDecimal(100)) >= 0) {
                numberToBeView = supportLimitValue.divide(new BigDecimal(100), 0, BigDecimal.ROUND_HALF_UP);
                return numberToBeView.toString() + BILLION_VALUE_KEYWORD;
            } else {
                return numberToBeView.toString() + MILLION_VALUE_KEYWORD;
            }
        }
        return NULL_VALUE_KEYWORD;
    }

}
