/*
 * Copyright (c) 2018 LINE Corporation. All rights reserved.
 * LINE Corporation PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */
package com.region.cooperation.api.filter;

import org.apache.commons.lang.CharEncoding;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.filter.CharacterEncodingFilter;

public class UTF8CharacterEncodingFilter extends CharacterEncodingFilter {

    public UTF8CharacterEncodingFilter() {
        super(CharEncoding.UTF_8, true);
        setBeanName(StringUtils.uncapitalize(CharacterEncodingFilter.class.getSimpleName()));
    }
}
