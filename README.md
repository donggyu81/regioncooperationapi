# 개발 프레임워크
## 언어 : 자바 1.8 
## 메인프레임워크 : Spring Boot 2.1
## ORM : Spring JPA + Hibernate
## DB : mysql
## 빌드 툴 : maven

# 문제 해결 방법

## 기본 정의

### 데이터 컬럼의 정의
문제에 지정된 대로 정의. 아래의 각 컬럼이 무조건 하나의 컬럼이 되어야 한다고 판단.

```
- 지자체 지원정보 엔티티: { ID, 지자체 코드, 지원대상, 용도, 지원한도, 이차보전,
추천기관, 관리점, 취급점, 생성일자, 수정일자 }
- 지원 지자체 (기관): { 지자체명, 지자체 코드 }
```

### 지원한도 및 이차보전의 추가 데이터 정의
지원 한도에서 "추천금액 이내"는, 금액 자체가 정해져 있지 않기 때문에 null 로 처리함.
따라서, 내림차순 정렬에서는 이를 반영하지 않음.
나머지는 전부 BigDecimal 형태의 숫자로 처리함.

1차로 DB에서 지원한도로 정리하고, 2차 질의 조건인 지원 금액이 동일한 경우에 대해서는 
Comparator 를 정의하여 처리함.


이차보전의 경우, "대출이자 전액"은 전액보전으로 보고 저장 시 100%로 저장함.
이차보전 비율이 최대인 것으로 가정함.
이차보전의 정렬은, 최소값을 1기준으로, 최대값을 2차 기준으로 보고 정렬함.
1차로 최소값이 작으면 작다고 판단하며, 최소값이 같은 경우 최대값이 작으면 작다고 판단함.
두 값이 완전히 동일하면 같은 비율로 봄.


## 테이블 데이터 정의

### 지원 지자체 정보 
```
CREATE TABLE `region_info` (
  `name` varchar(100) NOT NULL, // 지자체 명
  `code` varchar(20) NOT NULL, // 지자체 코드
  PRIMARY KEY (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
```

### 지자체 지원 정보 
```
CREATE TABLE `region_support_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT, // id
  `region_code` varchar(20) NOT NULL, /// 지자체 코드
  `support_target` varchar(500) DEFAULT NULL, // 지원 대상
  `support_usage` varchar(50) DEFAULT NULL, // 용도
  `support_limit` decimal(10,2) DEFAULT NULL, // 지원한도
  `second_preserve` varchar(20) DEFAULT NULL, // 이차보전
  `recommend_office` varchar(100) DEFAULT NULL, // 추천기관
  `manage_office` varchar(100) DEFAULT NULL, // 관리점
  `take_office` varchar(100) DEFAULT NULL, // 취급점
  `create_ymdt` datetime DEFAULT NULL, // 생성일자
  `last_mod_ymdt` datetime DEFAULT NULL, // 수정일자
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1046 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
```

### 사용자 정보
```
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT, // id
  `name` varchar(20) NOT NULL, // 사용자 id
  `password` varchar(500) NOT NULL, // 패스워드
  `create_ymdt` datetime NOT NULL, // 생성일자
  `last_mod_ymdt` datetime NOT NULL, // 수정일자
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci
```

### 전체 API 

### 데이터 저장 API
#### path : POST /uploadFile
#### input : file (multipart/form-data 형태로 전송)
#### output : json 
##### 동작 설명 
- 파일이 형식에 맞을 경우 데이터를 저장하며, 그렇지 않으면 exception 이 발생해 아래와 같이 응답이 나갑니다.
- 형식에 맞을 경우 데이터를 추가하는데, 만약 기존 데이터가 있다면 이를 저장하지 않습니다.
##### example 
```
// on success
{
	"returnCode" : "0000",
	"returnCode" : "Success",
}
```

```
// on error
{
	"returnCode" : "9999",
	"returnCode" : "Error",
}
```

### 전체 데이터 검색 API 
#### path : GET /searchAll
#### input parameter : N/A
#### output json
##### 동작 설명

- 데이터가 하나 이상 있을 경우 아래와 같이 데이터를 전달합니다.
```
{
    "returnCode": "0000",
    "returnMessage": "Success",
    "info": [
        {
            "region": "강릉시",
            "target": "강릉시 소재 중소기업으로서 강릉시장이 추천한 자",
            "usage": "운전",
            "limit": "추천금액 이내",
            "rate": "1%~2%",
            "institute": "강릉시",
            "mgmt": "강릉지점",
            "reception": "강릉시 소재 영업점"
        }
		...
    ]
}
```

- 데이터가 없으면 에러응답을 전달합니다.
```
{
	"returnCode" : "8888",
	"returnCode" : "Empty data"
}
```

### 지자체 검색 API
#### path : POST /findRegion
#### input : 지자체 명을 포함하는 json
```
{
“region”:”강릉시”
}
```

#### output : 지자체 정보
- 데이터가 있으면 아래와 같이 결과를 전달합니다.
```
{
    "returnCode": "0000",
    "returnMessage": "Success",
    "info": {
            "region": "강릉시",
            "target": "강릉시 소재 중소기업으로서 강릉시장이 추천한 자",
            "usage": "운전",
            "limit": "추천금액 이내",
            "rate": "1%~2%",
            "institute": "강릉시",
            "mgmt": "강릉지점",
            "reception": "강릉시 소재 영업점"
   }
}
```
- 데이터가 없으면 에러응답을 전달합니다.
```
{
	"returnCode" : "8888",
	"returnCode" : "Empty data"
}
```


### 지자체 정보 수정 API
#### path : POST /updateData
#### input : 지자체 명과, 수정 데이터를 포함하는 json
```
{ 
	"region" : "강릉시",
	"target" : "강릉시 소재 중소기업으로서 강릉시장이 추천한 자"
}
```

#### output : 수정된 데이터가 있으면 수정된 데이터에 해당하는 지역정보를 전달.
```
{
    "returnCode": "0000",
    "returnMessage": "Success",
    "info": {
            "region": "강릉시",
            "target": "강릉시 소재 중소기업으로서 강릉시장이 추천한 자",
            "usage": "운전",
            "limit": "추천금액 이내",
            "rate": "1%~2%",
            "institute": "강릉시",
            "mgmt": "강릉지점",
            "reception": "강릉시 소재 영업점"
   }
}
```
- 만약 전달된 지역명에 해당하는 데이터가 없으면 아래와 같은 에러 응답을 전달합니다.
```
{
	"returnCode" : "8888",
	"returnCode" : "Empty data"
}
```


### 지원금액으로 내림차순 정렬(지원금액이 동일하면 이차보전 평균 비율이 적은 순서)하여 특정 개수만 출력하는 API
#### path : GET /getRegionNames
#### input parameter 
- 결과 출력 개수 : (Integer) count 
#### output 
- 지원 금액이 가장 적은 지자체명의 목록
```
{
    "returnCode": "0000",
    "returnMessage": "Success",
    "info": {
        "regionNames": [
            "경기도",
            "제주도",
            "국토교통부",
            "인천광역시",
            "안양시",
            "안산시"
        ]
    }
}
```
- 만약 DB에 데이터가 하나도 없으면 아래와 같은 에러 응답을 전달합니다.
```
{
	"returnCode" : "8888",
	"returnCode" : "Empty data"
}
```

### 이차보전 컬럼에서 보전 비율이 가장 작은 추천 기관명을 출력하는 API
#### path : GET /getLeastPreserveInstitute
#### input parameter : N/A
#### output
- 이차보전 보전 비율이 가장 작은 추천 기관명
```
{
    "returnCode": "0000",
    "returnMessage": "Success",
    "info": {
        "regionNames": [
            "금천구"
        ]
    }
}
```
- 만약 DB에 데이터가 하나도 없으면 아래와 같은 에러 응답을 전달합니다.
```
{
	"returnCode" : "8888",
	"returnCode" : "Empty data"
}
```


### TC

####
|클래스 명|class coverage|method coverage|code coverage|
-------------------
|RegionSupportInfoService|100% (1/1)|100% (7/7)|63% (53/84)
|SecuredUserDetailService|100% (1/1)|100% (1/1)|100% (8/8)

# 빌드 및 실행
## 빌드
프로젝트 root 에서 mvn install 실행.

## 실행
- 나온 결과물 ( api-0.0.1-snapshot.jar ) 를 자바로 실행
- java -jar api-0.0.1-snapshot.jar
- 서버의 port 는 8080 입니다.
